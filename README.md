# FVar - Fast Vars for Haskell
This library implements an "FVar" or _fast Var_. The idea is that it allows concurrent readers
to access shared data without locking, but the moment a writer performs a write it gets exclusive
access to the var. If there are readers currently accesing the data, it waits until they complete
the operation, blocks any further reads and then it performs the write. Once it is done, it
reenables the stalled readers and lockless operation continues.

The implementation favours reads above writes. When reads are stalled to service a write,
all read operations stalled waiting for the write to complete are allowed to be performed
right after the one write is completed, regardless of whether there might be other writes
already waiting. After those pending reads are finished, another write will be allowed and 
thus new read requests will be blocked etc. This means the FVar lock is better suited for
applications where data is read by multiple threads often but seldom written to.

For API documentation see the [documentation on Hackage](http://hackage.haskell.org/package/fvars/docs/Control-Concurrent-FVar.html)